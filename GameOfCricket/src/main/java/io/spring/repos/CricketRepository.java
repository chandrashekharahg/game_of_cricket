package io.spring.repos;

import io.spring.models.MatchDetails;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CricketRepository extends MongoRepository<MatchDetails, String> {
}
