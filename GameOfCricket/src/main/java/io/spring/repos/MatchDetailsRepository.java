package io.spring.repos;

import io.spring.models.MatchDetails;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MatchDetailsRepository extends MongoRepository<MatchDetails, String> {

    @Query("{ 'matchId' : { $in: ?0 } }")
    public List<MatchDetails> findByMatchIdIn(List<String> matchIds);
}
