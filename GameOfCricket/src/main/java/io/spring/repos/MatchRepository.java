package io.spring.repos;

import io.spring.models.Match;
import io.spring.models.MatchDetails;
import io.spring.models.Tournament;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MatchRepository extends MongoRepository<Match, String> {
}

