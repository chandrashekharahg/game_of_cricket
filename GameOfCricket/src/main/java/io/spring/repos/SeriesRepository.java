package io.spring.repos;

import io.spring.models.SeriesDetails;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SeriesRepository extends MongoRepository<SeriesDetails, String> {
}
