package io.spring.repos;

import io.spring.models.Player;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository  extends MongoRepository<Player,String> {
}
