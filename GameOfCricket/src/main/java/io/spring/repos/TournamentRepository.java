package io.spring.repos;

import io.spring.models.Tournament;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TournamentRepository extends MongoRepository<Tournament, String>  {
}

