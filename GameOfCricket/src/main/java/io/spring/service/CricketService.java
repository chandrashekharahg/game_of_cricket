package io.spring.service;

import io.spring.Util;
import io.spring.models.*;
import io.spring.repos.CricketRepository;
import io.spring.repos.SeriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CricketService {


    @Autowired
    private CricketRepository cricketRepository;
    @Autowired
    private SeriesRepository seriesRepository;

    public MatchDetails startMatch(int overs) {
        MatchDetails matchDetails = new MatchDetails();
        List<Innings> inningsList = new ArrayList<>();

        for (int inningNumber = 1; inningNumber <= 2; inningNumber++) {
            Innings innings = new Innings();
//
//            innings.initializeBattingTeam();
//            innings.initializeBowlingTeam();
            int wicketCount = 0;


            for (int overNumber = 1; overNumber <= overs; overNumber++) {
                Over over = new Over();

                innings.addOver(over);


                for (int ballNumber = 1; ballNumber <= 6; ballNumber++) {
                    BallResult ballResult = Util.getRandomResult();
                    Ball ball = new Ball();
                    over.addBall(ball);
                    ball.setResult(ballResult);

                    if (wicketCount == 10) {
                        matchDetails.setInnings(inningsList);
                        cricketRepository.save(matchDetails);
                        return matchDetails;
                    }

                    if (ballResult == BallResult.RUNS) {
                        int runs = Util.getRandomRuns();
                        ball.setRuns(runs);
                        innings.addRun(runs);
                    } else if (ballResult == BallResult.WICKET) {
                        WicketType wicketType = Util.getRandomWicketType();
                        ball.setWicketType(wicketType);
                        wicketCount++;

                        innings.addWicket(wicketType);
                    } else if (ballResult == BallResult.NOBALL) {
                        int runs = Util.getRandomRuns() + 1;
                        ball.setRuns(runs);
                        innings.addRun(runs);
                        ballNumber--;
                    }

                }

//                innings.addOver(over);
            }

            inningsList.add(innings);
        }

        matchDetails.setInnings(inningsList);

        cricketRepository.save(matchDetails);

        return matchDetails;
    }


//    public void startSeries(int overs, int numberOfMatches) {
//        for (int matchNumber = 1; matchNumber <= numberOfMatches; matchNumber++) {
//            MatchDetails matchDetails = startMatch(overs);
//            saveSeriesDetails(matchDetails, matchNumber);
//        }
//    }

//    private void saveSeriesDetails(MatchDetails matchDetails, int matchNumber) {
//        SeriesDetails seriesDetails = SeriesDetails.builder()
//                .matchNumber(matchNumber)
//                .matchId(matchDetails.getId())
//                .build();
//        seriesRepository.save(seriesDetails);
//    }

    public MatchDetails getMatchDetails(String matchId) {
        return cricketRepository.findById(matchId).orElse(null);
    }
}
