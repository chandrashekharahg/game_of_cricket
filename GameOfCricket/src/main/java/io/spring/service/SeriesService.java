package io.spring.service;

import io.spring.models.Match;
import io.spring.models.SeriesDetails;
import io.spring.repos.SeriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SeriesService {

    @Autowired
    SeriesRepository seriesRepository;

    @Autowired
    TeamService teamService;

    @Autowired
    MatchService matchService;

    public SeriesDetails save(SeriesDetails seriesDetails) {
        boolean check = teamService.findByName(seriesDetails.getTeam1()) != null && teamService.findByName(seriesDetails.getTeam2()) != null && seriesDetails.getNumberOfMatch() > 0;
        return check ? seriesRepository.save(seriesDetails) : null;
    }

    public String startSeries(String seriesId) {
        SeriesDetails seriesDetails = findById(seriesId);
        if (seriesDetails == null) {
            return "Series not found";
        }
        doSeriesMatch(seriesDetails);
        return "Match Completed !";

    }

    private void doSeriesMatch(SeriesDetails seriesDetails) {
        List<Match> matches = new ArrayList<>();
        for (int i = 0; i < seriesDetails.getNumberOfMatch(); i++) {
            Match match = Match.builder().team1(seriesDetails.getTeam1()).team2(seriesDetails.getTeam2()).overs(seriesDetails.getOvers()).build();
            match = matchService.startMatch(match);
            matches.add(match);
            if (match.getWinner().equalsIgnoreCase(seriesDetails.getTeam1())) {
                seriesDetails.setTeam1WinningCount(seriesDetails.getTeam1WinningCount() + 1);
            } else {
                seriesDetails.setTeam2WinningCount(seriesDetails.getTeam2WinningCount() + 1);
            }

        }

        System.out.println(seriesDetails.getTeam1WinningCount());
        System.out.println(seriesDetails.getTeam2WinningCount());
        if (seriesDetails.getTeam1WinningCount() > seriesDetails.getTeam2WinningCount()) {
            seriesDetails.setWinningTeam(seriesDetails.getTeam1());
        } else if (seriesDetails.getTeam2WinningCount() > seriesDetails.getTeam1WinningCount()) {
            seriesDetails.setWinningTeam(seriesDetails.getTeam2());
        } else {
            seriesDetails.setWinningTeam("Draw");
        }
        seriesDetails.setMatches(matches);
        save(seriesDetails);
    }

    public SeriesDetails findById(String seriesId) {
        return seriesRepository.findById(seriesId).orElse(null);
    }
}
