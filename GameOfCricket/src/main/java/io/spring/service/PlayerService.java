package io.spring.service;

import io.spring.models.Player;
import io.spring.repos.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlayerService {

    @Autowired
    PlayerRepository playerRepository;

    public Player save(Player player) {
        return playerRepository.save(player);
    }

}
