package io.spring.service;

import io.spring.errorhandler.EntityNotFoundException;
import io.spring.models.*;
import io.spring.repos.MatchRepository;
import io.spring.repos.TeamRepository;
import io.spring.repos.TournamentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TournamentService {

    @Autowired
    private TournamentRepository tournamentRepository;

    @Autowired
    MatchService matchService;

    @Autowired
    MatchDetailsService matchDetailsService;


//    private List<Team> initializeTeams(int numberOfTeams) {
//        List<Team> teams = new ArrayList<>();
//
//        for (int i = 0; i < numberOfTeams; i++) {
//            teams.add(Team.builder().name("Team " + i).build());
//            teamRepository.saveAll(teams);
//        }
//
//        return teams;
//    }


    private List<Match> createMatches(List<String> teams, int overs) {
        List<Match> matches = new ArrayList<>();
        for (int i = 0; i < teams.size() - 1; i++) {
            for (int j = i + 1; j < teams.size(); j++) {
                Match match = Match.builder()
                        .team1(teams.get(i))
                        .team2(teams.get(j))
                        .overs(overs)
                        .build();
                matches.add(match);

            }
        }
        return matches;
    }

    public void startMatchInTournament(String tournamentId) throws EntityNotFoundException {
        Tournament tournament = tournamentRepository.findById(tournamentId)
                .orElseThrow(() -> new EntityNotFoundException("Tournament not found"));

        if (tournament.getStatus() != TournamentStatus.NOT_STARTED) {
            throw new IllegalStateException("Tournament is already in progress or completed.");
        }
        tournament.setStatus(TournamentStatus.IN_PROGRESS);
        tournamentRepository.save(tournament);
        List<String> matchIds = new ArrayList<>();
        for (int i = 0; i < tournament.getMatches().size(); i++) {
            Match match = tournament.getMatches().get(i);

            matchIds.add(matchService.startMatch(match).getId());
        }
       List<MatchDetails> matchDetails= matchDetailsService.findByMatchIdIn(matchIds);
        //tournament.setMatchDetails();
        System.out.println(matchIds);
        System.out.println(matchDetails.toString());
        tournament.setMatchDetails(matchDetails);
        tournamentRepository.save(tournament);
    }

    public Tournament getTournamentDetails(String tournamentId) throws EntityNotFoundException {
        return tournamentRepository.findById(tournamentId)
                .orElseThrow(() -> new EntityNotFoundException("Tournament not found with id: " + tournamentId));
    }


    public Tournament updateTournamentStatus(String tournamentId, TournamentStatus newStatus) throws EntityNotFoundException {
        Optional<Tournament> optionalTournament = tournamentRepository.findById(tournamentId);
        if (optionalTournament.isPresent()) {
            Tournament tournament = optionalTournament.get();
            tournament.setStatus(newStatus);
            tournamentRepository.save(tournament); // Save the updated tournament
            return tournament;
        } else {
            throw new EntityNotFoundException("Tournament not found with id: " + tournamentId);
        }
    }

    public Tournament save(Tournament tournament) {
        if (tournament.getTeamIds().size() < 2) {
            return null;
        }
        tournament.setMatches(createMatches(tournament.getTeamIds(), tournament.getOvers()));
        tournament.setStatus(TournamentStatus.NOT_STARTED);
        return tournamentRepository.save(tournament);
    }
}
