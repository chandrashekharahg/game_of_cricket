package io.spring.service;


import io.spring.Util;
import io.spring.models.*;
import io.spring.repos.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MatchService {

    @Autowired
    TeamService teamService;
    @Autowired
    MatchDetailsService matchDetailsService;
    @Autowired
    MatchRepository matchRepository;

    public Match saveAndStart(Match match) {
        if (teamService.findByName(match.getTeam1()) != null && teamService.findByName(match.getTeam2()) != null) {
            Match match1 = matchRepository.save(match);
            startMatch(match1);
            return match1;
        }
        return null;


    }

    public Match startMatch(Match match) {
//        Team team1 = teamService.findById(match.getTeam1Id());
//        Team team2 = teamService.findById(match.getTeam2Id());
        if (Util.doToss() == 0) {
            match.setTossWinner(match.getTeam1());
        } else {
            match.setTossWinner(match.getTeam2());
        }
        playMatch(match);
        return matchRepository.save(match);


    }


    private void playMatch(Match match) {
        List<Innings> inningsList = new ArrayList<Innings>();
        Innings i1 = simulateInnings(match.getTeam1(), match.getTeam2(), match.getOvers(), match.getTeam2Score());
        match.setTeam1Score(i1.getTotalRuns());
        Innings i2 = simulateInnings(match.getTeam2(), match.getTeam1(), match.getOvers(), match.getTeam1Score());
        match.setTeam2Score(i2.getTotalRuns());
        inningsList.add(i1);
        inningsList.add(i2);
        MatchDetails matchDetails = MatchDetails.builder().matchId(match.getId()).innings(inningsList).build();
        match.setWinner(determineMatchResult(matchDetails));
        match.setMatchDetails(matchDetailsService.save(matchDetails));
    }

    private String determineMatchResult(MatchDetails matchDetails) {
        List<Innings> inningsList = matchDetails.getInnings();
        int team1TotalRuns = inningsList.get(0).getTotalRuns();
        int team2TotalRuns = inningsList.get(1).getTotalRuns();
        if (team1TotalRuns > team2TotalRuns) {
            return inningsList.get(0).getBattingTeam().getName();
        } else if (team1TotalRuns < team2TotalRuns) {
            return inningsList.get(1).getBattingTeam().getName();
        } else {
            return null;
        }

    }

    private Innings simulateInnings(String team1, String team2, int overs, int target) {
        Team battingTeam = teamService.findByName(team1);
        Team bowlingTeam = teamService.findByName(team2);
        Innings innings = Innings.builder().battingTeam(battingTeam).bowlingTeam(bowlingTeam).build();
        int wicketCount = 0;
        for (int overNumber = 1; overNumber <= overs; overNumber++) {
            Over over = new Over();
            innings.addOver(over);

            Player currentBowler = bowlingTeam.getPlayers().get(innings.getCurrentBowlerIndex());

            for (int ballNumber = 1; ballNumber <= 6; ballNumber++) {
                BallResult ballResult = Util.getRandomResult();
                Ball ball = new Ball();
                over.addBall(ball);
                ball.setResult(ballResult);
                Player striker = innings.getCurrentStriker();
                Player nonStriker = innings.getNonStriker();

                if (ballResult == BallResult.RUNS) {
                    int runs = Util.getRandomRuns();
                    ball.setRuns(runs);
                    innings.addRun(runs);
                    innings.setTotalBalls(innings.getTotalBalls() + 1);
                    striker.setBallsFaced(striker.getBallsFaced() + 1);
                    over.setOverScore(over.getOverScore() + runs);
                    striker.setRunsScored(striker.getRunsScored() + runs);
                    if (runs % 2 != 0) {
                        innings.rotateStrike(false);
                    }
                    currentBowler.setBallsBowled(currentBowler.getBallsBowled() + 1);


                } else if (ballResult == BallResult.WICKET) {
                    WicketType wicketType = Util.getRandomWicketType();
                    ball.setWicketType(wicketType);
                    innings.addWicket(wicketType);
                    innings.setTotalBalls(innings.getTotalBalls() + 1);
                    currentBowler.setWicketsTaken(currentBowler.getWicketsTaken() + 1);
                    striker.setWicketTypes(wicketType);

                    wicketCount++;
                    if (wicketCount == 10) {
                        return innings;
                    } else {
                        innings.rotateStrike(true);
                        striker = innings.getCurrentStriker();
                        nonStriker = innings.getNonStriker();
                        battingTeam.getPlayers().add(nonStriker);
                        innings.setNonStriker(nonStriker);
                    }
                } else if (ballResult == BallResult.NOBALL) {
                    int runs = Util.getRandomRuns();
                    ball.setRuns(runs + 1);
                    innings.addRun(runs + 1);
                    over.setOverScore(over.getOverScore() + runs + 1);
                    ballNumber--;
                    striker.setRunsScored(striker.getRunsScored() + runs);
                    if (runs % 2 != 0) {
                        innings.rotateStrike(false);
                    }
                } else if (ballResult == BallResult.WIDE) {
                    innings.addRun(1);
                    ball.setRuns(1);
                    over.setOverScore(over.getOverScore() + 1);
                    ballNumber--;

                }



            }
            over.setOverCompleted(over.getBalls().size()>= 6 ? true : false);
            if (target > 0 && innings.getTotalRuns() > target) {
                return innings;
            }
            innings.rotateStrike(false);
            innings.rotateBowler();
        }
        return innings;

    }

    public Match findById(String matchId) {
        return matchRepository.findById(matchId).orElse(null);
    }
}
