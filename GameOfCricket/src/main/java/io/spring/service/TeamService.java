package io.spring.service;

import io.spring.models.Player;
import io.spring.models.Team;
import io.spring.repos.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TeamService {

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    PlayerService playerService;


    public Team findById(String teamId) {
        return teamRepository.findById(teamId).orElse(null);
    }

    public Team save(Team team) {
        List<Player> players = new ArrayList<>();
        for (int i = 1; i <= 11; i++) {
            Player player = Player.builder().name("Player " + i).build();
            playerService.save(player);
            players.add(player);
        }
        team.setPlayers(players);
        return teamRepository.save(team);

    }

    public List<Team> getTeams() {
        return teamRepository.findAll();
    }

    public Team findByName(String teamName) {
        return teamRepository.findByName(teamName);
    }
}
