package io.spring.service;

import io.spring.models.MatchDetails;
import io.spring.repos.MatchDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MatchDetailsService {

    @Autowired
    MatchDetailsRepository matchDetailsRepository;

    public MatchDetails save(MatchDetails matchDetails){
       return matchDetailsRepository.save(matchDetails);
    }


    public List<MatchDetails>findByMatchIdIn(List<String> matchIds){
        return matchDetailsRepository.findByMatchIdIn(matchIds);
    }


}
