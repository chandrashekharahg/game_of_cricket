package io.spring.errorhandler;

public class EntityNotFoundException extends Exception {
    public EntityNotFoundException(String tournament_not_found) {
        super(tournament_not_found);
    }
}
