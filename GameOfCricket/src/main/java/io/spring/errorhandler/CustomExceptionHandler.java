package io.spring.errorhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.MethodNotAllowedException;

@RestControllerAdvice
public class CustomExceptionHandler {
    @ExceptionHandler(MethodNotAllowedException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ResponseEntity<CustomErrorResponse> handleMethodNotAllowedException(MethodNotAllowedException ex) {
        CustomErrorResponse errorResponse = CustomErrorResponse.builder()
                .error(HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase())
                .message("Invalid request method for the /startMatch endpoint. Please use the correct HTTP method.")
                .build();

        return new ResponseEntity<>(errorResponse, HttpStatus.METHOD_NOT_ALLOWED);
    }
}
