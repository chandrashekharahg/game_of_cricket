package io.spring;

import io.spring.models.BallResult;
import io.spring.models.WicketType;

import java.util.Random;

public class Util {
    private static final Random random = new Random();

    public static boolean isEmpty(String value) {
        return value == null || value.isEmpty();
    }


//    public String getRandomResult() {
//        int randomNumber = random.nextInt(100);
//        if (randomNumber < 60) {
//            return String.valueOf(random.nextInt(7));
//        } else {
//            return "W";
//        }
//    }

    public static BallResult getRandomResult() {
        int randomNumber = random.nextInt(100);


        if (randomNumber < 70) {
            return BallResult.RUNS;
        } else if (randomNumber < 85) {
            return BallResult.WICKET;
        } else if (randomNumber < 90) {
            return BallResult.WIDE;
        } else {
            return BallResult.NOBALL;
        }

    }
    public static int getRandomRuns() {
        return random.nextInt(7);
    }

    public static WicketType getRandomWicketType() {
        return WicketType.values()[random.nextInt(WicketType.values().length)];
    }

    public static int doToss(){
       return random.nextInt(2);

    }
}
