package io.spring.models;

public enum BallResult {
    RUNS, WICKET, WIDE, NOBALL;
}
