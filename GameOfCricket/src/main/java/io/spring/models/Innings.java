package io.spring.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Innings {

    private List<Over> overs;
    private int totalRuns;

    private int totalBalls;
    private int totalWickets;
    private int currentStrikerIndex;
    @Builder.Default
    private int currentNonStrikerIndex=1;
    private Team battingTeam;
    private Team bowlingTeam;
    private List<Wicket> wickets;
    private int currentBowlerIndex;

    public int getCurrentBowlerIndex() {
        return currentBowlerIndex;
    }
    public void rotateBowler() {
        currentBowlerIndex = (currentBowlerIndex + 1) % bowlingTeam.getPlayers().size();
    }

    public void addRun(int run) {
        totalRuns += run;
    }

    public Player getNonStriker() {
        return battingTeam.getPlayers().get(currentNonStrikerIndex);
    }

    public void rotateStrike(boolean playerOut) {
        if (playerOut) {

            battingTeam.getPlayers().get(currentStrikerIndex).setOut(true);
            int temp = currentStrikerIndex;
            currentStrikerIndex = currentNonStrikerIndex;
            currentNonStrikerIndex = (temp + 1) % 11;

            System.out.println("stri o "+currentStrikerIndex);   System.out.println("stri non o "+currentNonStrikerIndex);

        } else {
            int temp = currentStrikerIndex;
            currentStrikerIndex = currentNonStrikerIndex;
            currentNonStrikerIndex = temp;
            System.out.println("stri"+currentStrikerIndex);   System.out.println("stri non "+currentNonStrikerIndex);

        }
    }

    public void setNonStriker(Player newNonStriker) {
        currentNonStrikerIndex = battingTeam.getPlayers().indexOf(newNonStriker);
    }
    public void addOver(Over over) {
        if (overs == null) {
            overs = new ArrayList<>();
        }
        overs.add(over);
        // setOvers(overs);
    }

    public Player getCurrentStriker() {
        return battingTeam.getPlayers().get(currentStrikerIndex);
    }

    public void addWicket(WicketType wicketType) {
        if (wickets == null) {
            wickets = new ArrayList<>();
        }
        Wicket wicket = Wicket.builder()
                .wicketType(wicketType)
                .build();
        wickets.add(wicket);
        totalWickets++; // Update total wickets
    }

    public List<Player> createPlayers() {
        List<Player> players = new ArrayList<>(11);
        for (int i = 1; i <= 6; i++) {
            players.add(Player.builder().name("Batsman" + i).isBatsman(true).build());
        }
        for (int i = 1; i <= 5; i++) {
            players.add(Player.builder().name("Bowler" + i).isBatsman(false).build());
        }
        if (!players.isEmpty()) {
            players.get(0).setStriker(true);
        }

        return players;
    }
//    public void initializeBattingTeam() {
//        this.battingTeam = Team.builder().players(createPlayers()).name("BattingTeam").build();
//
//    }
//
//    public void initializeBowlingTeam() {
//        this.bowlingTeam = Team.builder().players(createPlayers()).name("BowlingTeam").build();
//    }
}
