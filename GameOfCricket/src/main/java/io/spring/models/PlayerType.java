package io.spring.models;

public enum PlayerType {
    BATSMAN,
    BOWLER,
    ALL_ROUNDER,
    WICKET_KEEPER,
    CAPTAIN;
}
