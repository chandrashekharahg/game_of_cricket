package io.spring.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Match {

    private String id;
    private String team1;
    private String team2;
    private String winner;
    private String tossWinner;
    @Min(value = 1, message = "Overs must be at least 1")
    private int overs;
    private MatchDetails matchDetails;
    private int team1Score;
    private int team2Score;



}
