package io.spring.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SeriesDetails {
    private String id;
    private String team1;
    private String team2;

    @Min(value = 1, message = " numberOfMatch should more then 0")
    private int numberOfMatch;
    private List<Match> matches;
    private int team1WinningCount;
    private int team2WinningCount;
    private String winningTeam;
    private int overs;


}
