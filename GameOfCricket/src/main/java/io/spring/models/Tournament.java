package io.spring.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Tournament {
    @Id
    private String id;
    private String name;
    private TournamentStatus status;
    private List<String> teamIds;
    private List<Match> matches;
    private int overs;
    private List<MatchDetails>matchDetails;

}
