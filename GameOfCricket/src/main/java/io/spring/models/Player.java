package io.spring.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Player {
    @Id
    private String id;
    private String name;
    private boolean isBatsman;
    private boolean isStriker;
    private int runsScored;
    private int ballsFaced;
    private int wicketsTaken;
    private int ballsBowled;
    private WicketType wicketTypes;
    private PlayerType playerType;
    private boolean out;

}

