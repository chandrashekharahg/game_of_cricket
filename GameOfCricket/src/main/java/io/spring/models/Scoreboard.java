package io.spring.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Scoreboard {
    private int oversDone;
    private Team battingTeam;
    private Team bowlingTeam;
    private int totalRuns;
    private int totalWickets;
}
