package io.spring.models;

public enum TournamentStatus {
    NOT_STARTED, IN_PROGRESS, COMPLETED
}
