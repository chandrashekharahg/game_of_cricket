package io.spring.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Over {
    private List<Ball> balls;
    private boolean overCompleted;
    private int overScore;

    public boolean isOverCompleted() {
        return balls != null && balls.size() == 6;
    }

    public void addBall(Ball ball) {
        if (balls == null) {
            balls = new ArrayList<Ball>();
        }
        balls.add(ball);
    }
}
