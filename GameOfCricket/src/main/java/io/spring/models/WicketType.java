package io.spring.models;

public enum WicketType {
    BOWLED, RUN_OUT, CATCH, LBW;
}
