package io.spring.controller;


import io.spring.models.Team;
import io.spring.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teams")
public class TeamController {

    @Autowired
    private TeamService teamService;


    @PostMapping
    public ResponseEntity<Team> addTeam(@RequestBody Team team){
        return ResponseEntity.ok(   teamService.save(team));
    }


    @GetMapping
    public ResponseEntity<List<Team>> getTeams(@RequestBody Team team){
        return ResponseEntity.ok(teamService.getTeams());
    }


    @GetMapping("/findByName")
    public ResponseEntity<?> findTeamByName(@RequestParam String teamName) {
        Team team = teamService.findByName(teamName);

        if (team != null) {
            return ResponseEntity.ok(team);
        } else {
            String message = "Team with name " + teamName + " not found.";
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(message);
        }
    }



}
