package io.spring.controller;

import io.spring.errorhandler.EntityNotFoundException;
import io.spring.models.Tournament;
import io.spring.models.TournamentStatus;
import io.spring.service.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/tour")
public class TournamentController {
    @Autowired
    private TournamentService tournamentService;

    @PostMapping
    public ResponseEntity createTournament(
            @RequestBody Tournament tournament) {
        Tournament tournament1 = tournamentService.save(tournament);
        return ResponseEntity.ok().body(tournament1 != null ? tournament1 : "Can not able to create, more than 2 teams required ");
    }

    @PostMapping("/{tournamentId}/startMatch")
    public ResponseEntity<String> startMatchInTournament(
            @PathVariable String tournamentId
    ) throws EntityNotFoundException {
        tournamentService.startMatchInTournament(tournamentId);
        return ResponseEntity.ok("Match started successfully");
    }

    @GetMapping("/{tournamentId}")
    public ResponseEntity<Tournament> getTournamentDetails(@PathVariable String tournamentId) throws EntityNotFoundException {
        Tournament tournament = tournamentService.getTournamentDetails(tournamentId);

        return ResponseEntity.ok(tournament);
    }

    @PutMapping("/{tournamentId}/updateStatus")
    public ResponseEntity<Tournament> updateTournamentStatus(
            @PathVariable String tournamentId,
            @RequestParam String status) throws EntityNotFoundException {
        Tournament updatedTournament = tournamentService.updateTournamentStatus(tournamentId, TournamentStatus.valueOf(status));
        return ResponseEntity.ok(updatedTournament);
    }
}
