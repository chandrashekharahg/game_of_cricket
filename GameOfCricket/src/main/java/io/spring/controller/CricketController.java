package io.spring.controller;


import io.spring.models.MatchDetails;
import io.spring.service.CricketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.MethodNotAllowedException;

@RestController
public class CricketController {
    @Autowired
    private CricketService cricketService;

    @PostMapping("/startMatch")
    public ResponseEntity<String> startMatch(@RequestParam int overs) {
        cricketService.startMatch(overs);
        return ResponseEntity.ok("Match started successfully");
    }

//    @PostMapping("/startSeries")
//    public ResponseEntity<String> startSeries(@RequestParam int overs, @RequestParam int numberOfMatches) {
//       cricketService.startSeries(overs, numberOfMatches);
//        return ResponseEntity.ok("Series started successfully");
//    }


    @GetMapping("/matchDetails")
    public ResponseEntity<MatchDetails> getMatchDetails(@RequestParam String matchId) {
        MatchDetails matchDetails = cricketService.getMatchDetails(matchId);
        return ResponseEntity.ok(matchDetails);
    }
    @ExceptionHandler(MethodNotAllowedException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ResponseEntity<String> handleMethodNotAllowedException(MethodNotAllowedException ex) {
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED)
                .body("Invalid request method. Please use the correct HTTP method.");
    }
}
