package io.spring.controller;


import io.spring.models.Match;
import io.spring.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/match")
public class MatchController {
    @Autowired
    MatchService matchService;


    @PostMapping
    public ResponseEntity<Match> startMatch(@RequestBody Match match) {
        return ResponseEntity.ok().body(matchService.saveAndStart(match));
    }


   @GetMapping("/getMatch")
    public ResponseEntity<Match> getMatch(@RequestParam String matchId) {
        return ResponseEntity.ok().body(matchService.findById(matchId));
    }


}
