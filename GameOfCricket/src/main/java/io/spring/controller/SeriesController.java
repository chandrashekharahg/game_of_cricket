package io.spring.controller;

import io.spring.models.SeriesDetails;
import io.spring.service.SeriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/series")

public class SeriesController {
    @Autowired
    SeriesService seriesService;

    @PostMapping
    public ResponseEntity<SeriesDetails> createSeries(@RequestBody SeriesDetails seriesDetails) {
        return ResponseEntity.ok().body(seriesService.save(seriesDetails));
    }

    @PostMapping("/{seriesId}/start")
    public ResponseEntity<?> startSeries(@PathVariable String seriesId) {
        return ResponseEntity.ok().body(seriesService.startSeries(seriesId));

    }


    @GetMapping("/{seriesId}")
    public ResponseEntity<SeriesDetails> getSeriesDetails(@PathVariable String seriesId) {
        return ResponseEntity.ok().body(seriesService.findById(seriesId));

    }
}
